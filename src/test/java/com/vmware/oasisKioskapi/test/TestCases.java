/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 */
package com.vmware.oasisKioskapi.test;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.jayway.restassured.response.Response;
import com.vmware.oasisKioskapi.utils.ExtentMarkup;
import com.vmware.oasisKioskapi.SchemaValidation;
import com.vmware.oasisKioskapi.utils.BaseApi;
import com.vmware.oasisKioskapi.utils.Constants;
import com.vmware.oasisKioskapi.utils.ExtentManager;
import com.vmware.oasisKioskapi.utils.ExtentTestCase;
import com.vmware.oasisKioskapi.utils.RestAssuredUtility;

/**
 * Execute all test cases.
 * 
 * @author Mindstix
 */
public class TestCases {

  private static final Logger LOGGER = LoggerFactory.getLogger(TestCases.class);
  private BaseApi baseapi = new BaseApi();
  private SchemaValidation schemaValidation = new SchemaValidation();
  private Response response;
  
  private int responseStatusCode;

  /**
   * set the Base URI and Path
   * 
   */
  @BeforeClass(groups = { "regression", "smoke" })
  public void setup() {
    LOGGER.info("Set up base url and base path for oasis api");
    RestAssuredUtility.setBaseUri(SuiteSetUpTest.vmwareEnvProps.getProperty("baseURL"));
    RestAssuredUtility.setBasePath(SuiteSetUpTest.vmwareEnvProps.getProperty("basePath"));
    ExtentTestCase.setExtent(ExtentManager.getExtent());

  }

  /**
   * Contains a endpoint url list of oasis api.
   * 
   */

  @DataProvider(name = "oasisKioskApiList")
  public Object[][] oasisApiList() throws InterruptedException {
    LOGGER.info("In API DataProvider");
    return Constants.apiName;
  }

  /**
   * Contains a wrong endpoint url list of oasis api.
   * 
   */
  @DataProvider(name = "falseEndpointUrl")
  public Object[][] falseApiName() throws InterruptedException {
    LOGGER.info("In API DataProvider");
    return Constants.falseApiName;
  }


  /**
   * To verify Resposne status code , response time and schema validation for all APIs.
   */
  @Test(groups = { "smoke",
      "regression" }, dataProvider = "oasisKioskApiList", priority = 2, enabled = true)
  public void verifyResponseCodeTimeAndSchemaValidation(String apiName, String extendedUrl) {
    LOGGER.info(" Executing 2nd test case ");
    schemaValidation.jsonSchemaValidationForRestApi(apiName, extendedUrl);
    LOGGER.info(" Executed 2nd test case ");
  }

  /**
   * To check 401 (Unauthorized) status code.
   */
  @Test(groups = { "regression" }, dataProvider = "oasisKioskApiList", priority = 3, enabled = true)
  public void verifyUnauthorizedStatusCodes(String apiName, String extendedUrl) {
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest(
        "Verify Unauthorized 401 Status Code for Api  " + apiName,
        " Endpoint Url is : " + extendedUrl));
    LOGGER.info(" Executing 3rd test case ");
    response = baseapi.getResponseForAllTheApi(Constants.UNAUTH, apiName, extendedUrl);
    responseStatusCode = baseapi.getStatusCode(response, apiName, extendedUrl);
    ExtentTestCase.getTest().info("Actual Response Status Code : " + responseStatusCode
        + "  Expected Response Status Code :  " + Constants.STATUSCODE_401);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_401, "Status code should be 401");
    LOGGER.info(" Executed 3rd test case ");
  }

  /**
   * To verify 404 (Page Not Found) status code.
   */
  @Test(groups = { "regression" }, dataProvider = "falseEndpointUrl", priority = 4, enabled = true)
  public void verifyPageNotFoundStatusCodes(String apiName, String extendedUrl)
      throws JsonParseException, JsonMappingException, IOException {
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest(
        "Verify 404 Status Code for Api  " + apiName, " Endpoint Url is : " + extendedUrl));
    LOGGER.info(" Executing 4th test case ");
    response = baseapi.getResponseForAllTheApi(Constants.API_METHOD_GET, apiName, extendedUrl);
    responseStatusCode = baseapi.getStatusCode(response, apiName, extendedUrl);
    ExtentTestCase.getTest().info("Actual Response Status Code : " + responseStatusCode
        + " Expected Response Status Code : " + Constants.STATUSCODE_404);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_404, "Status code should be 404");
    LOGGER.info(" Executed 4th test case ");
  }

  /**
   * AfterTest Method to get the Test Result.
   */
  @AfterMethod(groups = { "regression", "smoke" })
  public void getResult(ITestResult result) {
    LOGGER.info("Recording Results");
    if (result.getStatus() == ITestResult.FAILURE) {
      ExtentTestCase.getTest().fail(Status.FAIL + ", Failed TestCase is " + result.getName());
      ExtentTestCase.getTest().fail(Status.FAIL + ", Failed TestCase is " + result.getThrowable());
      ExtentMarkup.getMarkupCyanLabel("TestCase Failed ");
    } else if (result.getStatus() == ITestResult.SKIP) {
      ExtentTestCase.getTest().skip(Status.SKIP + ", Skipped TestCase is " + result.getName());
    } else {
      ExtentTestCase.getTest().pass(Status.PASS + " , Passed TestCase is : " + result.getName());
      ExtentMarkup.getMarkupBlueLabel("TestCase Passed ");
    }
    ExtentTestCase.getExtent().flush();
  }

  /**
   * Reset base URI and Base path.
   */
  @AfterClass
  public void tearDown() {
    RestAssuredUtility.resetBaseUri();
    RestAssuredUtility.resetBasePath();
  }
}
