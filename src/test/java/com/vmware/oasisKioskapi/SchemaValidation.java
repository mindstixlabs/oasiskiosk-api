/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.oasisKioskapi;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import java.io.FileNotFoundException;
import java.io.FileReader;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.jayway.restassured.response.Response;
import com.vmware.oasisKioskapi.utils.ExtentMarkup;
import com.vmware.oasisKioskapi.test.SuiteSetUpTest;
import com.vmware.oasisKioskapi.utils.BaseApi;
import com.vmware.oasisKioskapi.utils.BearerToken;
import com.vmware.oasisKioskapi.utils.Constants;
import com.vmware.oasisKioskapi.utils.ExtentTestCase;
import com.vmware.oasisKioskapi.utils.HeadersParamsConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

/**
 * JSON Schema validation. 
 * @author Mindstix.
 *
 */

public class SchemaValidation {
  private static final Logger LOGGER = LoggerFactory.getLogger(SchemaValidation.class);
  private Response response;
  private Long responseTime;
  private BaseApi baseapi = new BaseApi();
  
  /**
   *  JSON Schema validation.
   */
  public void jsonSchemaValidationForRestApi(String apiName, String endpointUrl) {
    if (apiName.contains(Constants.API_METHOD_GET)) {
      LOGGER.info("Executing Schema validation for GET Method");
      jsonSchemaValidationForGetRequestCall(apiName, endpointUrl);
    } else if (apiName.contains(Constants.API_METHOD_POST)) {
      LOGGER.info("Executing Schema validation for POST Method");
      jsonSchemaValidationForPostRequestCall(apiName, endpointUrl);
    }
  }

  /**
   * Schema validation for Get Api call.
   * 
   * @param apiName
   *          : name of the api.
   * @param endpointUrl
   *          : endpoint url of the api.
   * @param schemaFileName
   *          : json scehma file name
   */
  private void jsonSchemaValidationForGetRequestCall(String apiName, String endpointUrl) {
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest("Verify Response Status Code , Response time and Schema Validation for :" 
          + apiName));
    LOGGER.info("File Path : {} ", apiName);
    LOGGER.info("File Path: {} ", endpointUrl);
    String requestType = apiName.substring(0, apiName.indexOf("_"));
    String schemaFileName = apiName.substring(apiName.lastIndexOf("_") + 1);
    LOGGER.info("Schema File Name: {} ", schemaFileName);
    String schemaFilePath ="jsonFiles/"+schemaFileName+".json";
    LOGGER.info("SCHEMA FILE PATH : {}",schemaFilePath);
    LOGGER.info(" In get request call ");
    if (!schemaFileName.isEmpty()) {
      LOGGER.info("Comparing JSON Schema");
      response = given().log().all()
          .header(Constants.AUTHORIZATION,
              HeadersParamsConstants.AUTHORIZATION_ALEART + BearerToken.getAccessToken())
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
          .header(Constants.X_Remote_User, HeadersParamsConstants.X_Remote_User)
          .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT)
          .when().get(endpointUrl).then()
          .statusCode(Constants.STATUSCODE_200).and()
          .body(matchesJsonSchemaInClasspath(schemaFilePath)).extract()
          .response();
      extentReportForApi( requestType ,  schemaFileName,  endpointUrl);
      responseTime = baseapi.getResponseTime(response, apiName);
      Assert.assertTrue(baseapi.isResponseTimeValid,
          "Response Time Should be less than or equal to 300ms");
    } else {
      ExtentTestCase.getTest().info("Schema File Not Present");
      LOGGER.info("Schema File Not Present");
    }
  }
  
  /**
   * Extent report for status code and api name : To print apiname and status code and response body.
   */
  public void extentReportForApi(String requestType , String schemaFileName, String endpointUrl) {
    ExtentMarkup.getMarkupBlueLabel("Request Type is : " + requestType + " And Api Name is : " + schemaFileName);
    ExtentMarkup.getMarkupBlueLabel("Service Endpoint Url : " +endpointUrl);
    ExtentTestCase.getTest().info("Actual Response Status : " + response.statusCode()
        + " Expected Response Status Code : " + Constants.STATUSCODE_200);
    ExtentTestCase.getTest().pass("JSON Schema matched");
    LOGGER.info("JSON Schema matched ");
    ExtentMarkup.getMarkupCodeBlock("[Passed] API Response: " + response.body().prettyPrint());    
  }

  /**
   * Schema Validation for post Api call.
   * 
   * @param apiName
   *          : name of the api.
   * @param endpointUrl
   *          : endpoint url of the api.
   * @param schemaFileName
   *          : json scehma file name
   */
  private void jsonSchemaValidationForPostRequestCall(String apiName, String extendedUrl) {
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest("Verify Response Status Code , Response time and Schema Validation for :"
          + apiName)); 
    LOGGER.info("In post request call");
    LOGGER.info("File Path : {} ", apiName);
    LOGGER.info("File Path: {} ", extendedUrl);
    String requestType = apiName.substring(0, apiName.indexOf("_"));
    String apiNameJson = apiName.substring(apiName.lastIndexOf("_") + 1);
    String schemaFilePath ="jsonFiles/"+apiNameJson+".json";
    LOGGER.info(" Api Name : {} Schema File Path : {} ",apiNameJson,schemaFilePath);
    JsonParser json = new JsonParser();
    JsonObject jsonObj = null;
    try {
      jsonObj = (JsonObject) json
          .parse(new FileReader(SuiteSetUpTest.vmwareEnvProps.getProperty(apiNameJson)));
    } catch (JsonIOException e) {
      LOGGER.error("Error occured. ", e);
    } catch (JsonSyntaxException e) {
      LOGGER.error("Error occured. ", e);
    } catch (FileNotFoundException e) {
      LOGGER.error("Error occured. ", e);
    }
    String payload = null;
    if (jsonObj != null) {
      payload = jsonObj.toString();
    }
    LOGGER.info("Payload: {} ", payload);
    if (!schemaFilePath.isEmpty()) {
      LOGGER.info("Comparing JSON Schema");
      response = given().log().all()
          .header(Constants.AUTHORIZATION,
              HeadersParamsConstants.AUTHORIZATION_ALEART + BearerToken.getAccessToken())
          .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT)
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
          .header(Constants.X_Remote_User, HeadersParamsConstants.X_Remote_User)
          .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT)
          .body(payload).when()
          .post(extendedUrl).then().statusCode(Constants.STATUSCODE_200).and()
          .body(matchesJsonSchemaInClasspath(schemaFilePath)).extract().response();    
      extentReportForApi(requestType,apiNameJson,extendedUrl);
      ExtentMarkup.getMarkupCodeBlock("Request Body : " +payload);
      responseTime = baseapi.getResponseTime(response, apiName);
      Assert.assertTrue(baseapi.isResponseTimeValid,
          "Response Time Should be less than or equal to 300ms");
    } else {
      ExtentTestCase.getTest().info("Schema File Not Present");
      LOGGER.error("Schema File Not Present");
    }
  }
}
