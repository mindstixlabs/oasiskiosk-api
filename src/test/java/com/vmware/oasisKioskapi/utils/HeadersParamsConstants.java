/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.oasisKioskapi.utils;

/**
* Setting params and headers
* 
* @author Mindstix.
*
*/
public class HeadersParamsConstants {

	public final static String REDIRECT_URL = "http://mbei-dev.vmware.com:8050/oauth-server/access_response";
	public final static String SCOPE = "read";
	public final static String AUTHORIZATION = "Basic aG5fb2FzaXNfbW9iaWxlOnRlc3QxMjM=";	
	public final static String AUTH_VALUE = "Basic b2FzaXN1c2VyOnZtd2FyZQ==";
	public final static String AUTHORIZATION_ALEART = "Bearer ";
	public final static String CONTENT_TYPE = "application/json";
	public final static String ACCEPT = "application/json";
	public final static String X_Remote_User="sharmaankita";
	 public static final String WRONG_ACCEPT_VALUE = "AAA";
}
