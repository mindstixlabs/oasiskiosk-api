/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.oasisKioskapi.utils;

import static com.jayway.restassured.RestAssured.given;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jayway.restassured.response.Response;
import com.vmware.oasisKioskapi.test.SuiteSetUpTest;
import com.vmware.oasisKioskapi.utils.BearerToken;
import com.vmware.oasisKioskapi.utils.Constants;
import com.vmware.oasisKioskapi.utils.ExtendedUri;
import com.vmware.oasisKioskapi.utils.HeadersParamsConstants;
import com.vmware.oasisKioskapi.utils.RestAssuredUtility;

/**
 * Login and get the Authentication code
 * 
 * @author Mindstix
 * 
 */

public class BearerToken {

  // Declaring all the private and public members
  private static final Logger LOGGER = LoggerFactory.getLogger(BearerToken.class);
  private static String refreshToken;
  private static String accessToken;


  public static String getRefreshToken() {
    return refreshToken;
  }

  public static void setRefreshToken(String refreshToken) {
    BearerToken.refreshToken = refreshToken;
  }

  public static void setAccessToken(String accessToken) {
    BearerToken.accessToken = accessToken;
  }

  public static String getAccessToken() {
    return accessToken;
  }

  /**
   * Generate refresh token and access token.
   */
  public void generateToken() {
    // extract request access token from the code.
    String accessTokenUrl = SuiteSetUpTest.vmwareEnvProps.getProperty("baseURI_Token")
        + SuiteSetUpTest.vmwareEnvProps.getProperty("basePath_Token")
        + ExtendedUri.EXTENDED_ACCESS_TOKEN_URI_1 
        + ExtendedUri.EXTENDED_ACCESS_TOKEN_URI_2;
    LOGGER.info("acessTokenURL: {}", accessTokenUrl);
    Response res;
    // @formatter:off
    res = given()
        .log()
        .all()
        .header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION)
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
        .when()
        .post(accessTokenUrl)
        .then()
        .statusCode(Constants.STATUSCODE_200)
        .extract()
        .response();
    LOGGER.info("Response is : {} ", res.toString());
    // @formatter:on
    // get refresh token
    setRefreshToken(res.getBody().jsonPath().getString("refresh_token"));
    LOGGER.info("Refersh Token : {} ", getRefreshToken());
    setAccessToken(res.getBody().jsonPath().getString("access_token"));
    LOGGER.info("Access Token: {} ", getAccessToken());
  }

  /**
   * Quit driver, Reset base URI and Base path.
   */
  public void tearDown() {
    LOGGER.info("tearDown");
    RestAssuredUtility.resetBaseUri();
    RestAssuredUtility.resetBasePath();
  }

}
