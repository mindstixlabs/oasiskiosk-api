/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 */
package com.vmware.oasisKioskapi.utils;

/**
 * Constants.
 * @author Mindstix.
 */
public class Constants {

  public static final String USER_NAME = "ZHZpbmNodXJrYXI=";
  public static final String USER_PASSWORD = "TWluZHN0aXhAMTIz";
	public final static String X_Remote_User="X-Remote-User";
	public final static String SCOPE = "scope";
	public final static String AUTHORIZATION = "Authorization";
	public final static String CONTENT_TYPE = "Content-Type";
	public final static String ACCEPT = "Accept";
  public static final String DROPDOWN_ID = "userStoreDomain";
  public static final String DROPDOWN_VALUE = "vmware.com";
  public static final String NEXT_ID = "userStoreFormSubmit";
  public static final String API_METHOD_GET = "GET";
  public static final String API_METHOD_POST = "POST";
  public static final String API_METHOD_PUT = "PUT";
  public static final String REDIRECT_URI = "redirect_uri";
  public static final String UNAUTH = "UNAUTHORIZED";
  public static final String UNAUTH_POST = "unauthorizedPost";
  public static final String NOT_ACCEPT_GET = "notAcceptableGet";
  public static final String NOT_ACCEPT_POST = "notAcceptablePost";
  public static final String NEW_ACCESS_TOKEN = "newAccessToken";
  public static final String METHOD_NOT_ALLOWED = "methodNotAllowed";
  public static final String METHOD_NOT_ALLOWED_POST = "methodNotAllowedPost";
  public static final int EXP_RESP_TIME3 = 300;
  public static final int EXP_RESP_TIME5 = 5000;
  public static final int EXP_RESP_TIME10 = 10000;
  public static final int STATUSCODE_400 = 400;
  public static final int STATUSCODE_401 = 401;
  public static final int STATUSCODE_404 = 404;
  public static final int STATUSCODE_405 = 405;
  public static final int STATUSCODE_406 = 406;
  public static final int STATUSCODE_200 = 200;

	
	public static String[][] apiName = { 
	    {"GET_HelpDeskLocations","/oasis/helpdesk"},
	    {"GET_UserNameInfo","/user/user/sharmaankita"},
	//    {"POST_CreateWalkinTicket","/ticket/ticket"}			
	};
	
	public static String[][] falseApiName = { 
	    {"GET_HelpDesk Locations","/oasis/helpesk"},
      {"GET_UserName Info","/user/usr/sharmaankita"},
      {"POST_CreateWalkinTicket","/ticket/ticke"}  
  };
}
