/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.oasisKioskapi.utils;

/**
 * Setting extended URIs
 * 
 * @author Mindtsix.
 *
 */

public class ExtendedUri {
	
	public final static String EXTENDED_LOGIN_URI = "/authorize?response_type=code&redirect_uri=https://mbei.vmware.com:8443/oauth-server/access_response&client_id=er_app&idp=vidm";
	public final static String EXTENDED_ACCESS_TOKEN_URI_1 ="/token?grant_type=";
	public final static String EXTENDED_ACCESS_TOKEN_URI_2 ="password&username=svc.np_hn_oasis@vmware.com&password=7TT58P27R1QnDiuquop&scope=read&client_id=hn_oasis_mobile";
}



