/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 */
package com.vmware.oasisKioskapi.utils;

import static com.jayway.restassured.RestAssured.given;
import java.io.FileNotFoundException;
import java.io.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.jayway.restassured.response.Response;
import com.vmware.oasisKioskapi.utils.ExtentMarkup;
import com.vmware.oasisKioskapi.test.SuiteSetUpTest;
import com.vmware.oasisKioskapi.utils.BaseApi;
import com.vmware.oasisKioskapi.utils.BearerToken;
import com.vmware.oasisKioskapi.utils.Constants;
import com.vmware.oasisKioskapi.utils.ExtentTestCase;
import com.vmware.oasisKioskapi.utils.HeadersParamsConstants;

/**
 * Contains response, response status code , response time.
 * 
 * @author Mindstix
 *
 */

public class BaseApi {
  private static final Logger LOGGER = LoggerFactory.getLogger(BaseApi.class);
  private Response response;
  protected Long responseTime;
  private String newAccessToken;
  public Boolean isResponseTimeValid;
  private String newRefreshToken;
  public boolean isNewAccessToken = false;

  public enum HttpMethodName {
    GET, POST, UNAUTHORIZED;
  }

  /**
   * This method is used to get response for all the APIs.
   */
  public Response getResponseForAllTheApi(String webApiMethodName, String apiName,
      String extendedUrl) {
    LOGGER.info("File Path: {}", apiName);
    LOGGER.info("File Path: {}", extendedUrl);
    LOGGER.info("Method name: {}", webApiMethodName);
    HttpMethodName httpMethodName = HttpMethodName.valueOf(webApiMethodName);
    switch (httpMethodName)
    {
      case GET:
        LOGGER.info("In get request call");
        // Api for 200 , 404, 405 status code.
        response = getResponseOfGetRequestCall(apiName, extendedUrl);
        responseTime = response.time();
        LOGGER.info("Response time of {} : is {} ms ", apiName, responseTime);
        break;
      case POST:
        LOGGER.info("In post request call");
        // Api for 200 , 404, 405 status code.
        response = getResponseOfPostRequestCall(apiName, extendedUrl);
        responseTime = response.time();
        LOGGER.info("Response time of {} : is {} ms ", apiName, responseTime);
        break;
      case UNAUTHORIZED:
        LOGGER.info("Get call for unauthorized api");
        response = getUnauthorizedResponse(apiName, extendedUrl);
        break;
      default:
        LOGGER.error("Invalid Request Type");
    }
    return response;
  }

  /**
   * This method is used to get response for get call. For new access token and
   * old access token.
   * 
   * @return : response
   */
  private Response getResponseOfGetRequestCall(String getCallApiName, String getCallEndPointUrl) {
    if (isNewAccessToken) {
      LOGGER.info(" Executing for new Access token ");
      response = given().log().all()
          .header(Constants.AUTHORIZATION,
              HeadersParamsConstants.AUTHORIZATION_ALEART + newAccessToken)
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
          .header(Constants.X_Remote_User, HeadersParamsConstants.X_Remote_User)
          .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT).when().get(getCallEndPointUrl)
          .then().extract().response();
    } else {
      LOGGER.info(" Executing for older Access token ");
      response = given().log().all()
          .header(Constants.AUTHORIZATION,
              HeadersParamsConstants.AUTHORIZATION_ALEART + BearerToken.getAccessToken())
          .header(Constants.X_Remote_User, HeadersParamsConstants.X_Remote_User)
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
          .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT).when().get(getCallEndPointUrl)
          .then().extract().response();
    }
    LOGGER.info("Response of  : {} ", response.body().prettyPrint());
    return response;
  }

  /**
   * This method is used to get response for get call without passing bearer
   * token.
   * 
   * @param unauthorizedApiName
   *          : Name of the Api.
   * @param unauthorizedEndpointUrl
   *          :Endpoint Url of the Api.
   * @return : response
   */
  private Response getUnauthorizedResponse(String unauthorizedApiName,
      String unauthorizedEndpointUrl) {
    LOGGER.info("unauthorizedEndpointUrl : {} ", unauthorizedEndpointUrl);
    response = given().log().all().header(Constants.AUTHORIZATION, "")
        .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT).when().get(unauthorizedEndpointUrl)
        .then().extract().response();
    LOGGER.info("Response is : {} ", response.body().prettyPrint());
    return response;
  }

  /**
   * This method is used to get response for post call.
   * 
   * @apiName : Name of the api .
   * @endpointUrl : Endpoint url of the api.
   * @return : response
   */
  private Response getResponseOfPostRequestCall(String apiName, String endpointUrl) {
    LOGGER.info("Post Call for {} ", apiName);
    String apiNameJson = apiName.substring(apiName.lastIndexOf("_") + 1);
    LOGGER.info("apiNameJson : {}", apiNameJson);
    JsonParser json = new JsonParser();
    JsonObject jsonObj = null;
    try {
      jsonObj = (JsonObject) json
          .parse(new FileReader(SuiteSetUpTest.vmwareEnvProps.getProperty(apiNameJson)));
    } catch (JsonIOException e) {
      LOGGER.error("Error occured. ", e);
    } catch (JsonSyntaxException e) {
      LOGGER.error("Error occured. ", e);
    } catch (FileNotFoundException e) {
      LOGGER.error("Error occured. ", e);
    }
    String payload = null;
    if (jsonObj != null) {
      payload = jsonObj.toString();
    }
    LOGGER.info("Payload: \n {} ", payload);
    response = given().log().all()
        .header(Constants.AUTHORIZATION,
            HeadersParamsConstants.AUTHORIZATION_ALEART + BearerToken.getAccessToken())
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
        .header(Constants.X_Remote_User, HeadersParamsConstants.X_Remote_User)
        .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT).body(payload).when()
        .post(endpointUrl).then().extract().response();
    LOGGER.info("Status Code: {}", response.statusCode());
    return response;
  }

  /**
   * This method is used to get status code for all the APIs.
   */
  public int getStatusCode(Response response, String apiName, String endpointUrl) {
    String responseStatusCode = Integer.toString(response.statusCode());
    LOGGER.info("Calling : {} Api  : Response status code : {} ", apiName, responseStatusCode);
    String requestType = apiName.substring(0, apiName.indexOf("_"));
    apiName = apiName.substring(apiName.lastIndexOf("_") + 1);
    ExtentMarkup
        .getMarkupBlueLabel("Request Type is : " + requestType + " And Api Name is : " + apiName);
    ExtentMarkup.getMarkupBlueLabel("Service Endpoint Url : " + endpointUrl);
    ExtentTestCase.getTest().info("Response Status Code : " + responseStatusCode);
    ExtentMarkup.getMarkupCodeBlock("Api Response : " + response.body().prettyPrint());
    return response.statusCode();
  }

  /**
   * This method is used to get response time for all the APIs and validate
   * response time.
   */
  public Long getResponseTime(Response response, String apiName) {
    responseTime = response.time();
    LOGGER.info("Response Time  : {} ", responseTime);
    if (responseTime <= Constants.EXP_RESP_TIME3) {
      isResponseTimeValid = true;
      ExtentTestCase.getTest().info("API Response Time = " + responseTime + " ms");
      LOGGER.info("Api Resonse time is valid and its less than or equal to 300ms : {}", apiName);
    } else {
      isResponseTimeValid = false;
      ExtentTestCase.getTest()
          .fail("Api Response Time is more than 300ms : " + responseTime + " ms");
      LOGGER.info("Api Resonse time is more than 300ms : {} ", apiName);
    }
    return responseTime;
  }
}
